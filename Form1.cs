﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Compression;
namespace DeployToServer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DoZipFile();
        }
        private void DoZipFile()
        {
            try
            {
                folderBrowserDialog1.ShowDialog();
                if (!string.IsNullOrEmpty(folderBrowserDialog1.SelectedPath))
                {
                    var pathFile = folderBrowserDialog1.SelectedPath;                   
                    saveFileDialog1.DefaultExt = "zip";
                    saveFileDialog1.ShowDialog();
                    if (!string.IsNullOrEmpty(saveFileDialog1.FileName))
                    {
                        var pathsaveFile = saveFileDialog1.FileName;
                        ZipFile.CreateFromDirectory(pathFile, pathsaveFile);
                        setStatusBar(10,"...Loading");
                    }
                }
                
            }
            catch (Exception)
            {

                throw;
            }
        }
        private void setStatusBar(int status =10,string text ="")
        {
            toolStripProgressBar1.Value = status;
            toolStripProgressBar1.ToolTipText = "Running";
            toolStripStatusLabel1.Text = text;
        }
        public int SendFile()
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://www.contoso.com/test.htm");
            request.Method = WebRequestMethods.Ftp.UploadFile;

            // This example assumes the FTP site uses anonymous logon.
            request.Credentials = new NetworkCredential("anonymous", "janeDoe@contoso.com");

            // Copy the contents of the file to the request stream.
            byte[] fileContents;
            using (StreamReader sourceStream = new StreamReader("testfile.txt"))
            {
                fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
            }

            request.ContentLength = fileContents.Length;

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(fileContents, 0, fileContents.Length);
            }

            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            {
                Console.WriteLine($"Upload File Complete, status {response.StatusDescription}");
            }
            return 0;
        }
    }
}
